# README #

### What is this repository for? ###

This is an implementation of [Data Poisoning Attacks on Factorization-Based Collaborative Filtering (Bo Li etc. 2016 NIPS)]:https://arxiv.org/pdf/1608.08182.pdf

# Reference #
1.[Low-rank Matrix Completion using Alternating Minimization]:https://arxiv.org/pdf/1212.0467.pdf
1.1[code implementation]:https://github.com/vivkul/AltMinComplete/blob/master/AltMinCompleteRandom.py
2.[Singular value thresholding algorithm for matrix completion]:https://arxiv.org/pdf/0810.3286.pdf