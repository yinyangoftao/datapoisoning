import numpy as np
from scipy.sparse import coo_matrix
# from scipy.sparse.linalg import svds
import argparse
from util import *
from svt_pysp import *
from util_np import *
from altmin import altmin

#FIXME configure that the sigma bounding mechanism is reasonable
#modify Mf to be dense in attack function
def init_M_fake_SGLD(M, O, m_fake, sig_lb = 1e-2):
	"""
	sig_lb: lower bound of sigma. otherwise the inverse Sigma matrix would have NaN (when every rating of this item is identical)
	"""
	Md = dense_mat(M,O)
	m, n = O.shape
	si = np.mean(Md, axis = 0)
	Si = np.tile(si.reshape(1,-1), [m,1])
	sig_sq = np.mean((Md - Si)**2, axis = 0) #array(list(map(lambda j:np.sqrt(np.mean((Md[:,j] - si[j])**2)), range(n))))
	#sampling the columns of Mf
	sig_sq[sig_sq < sig_lb] = sig_lb
	Mf = np.hstack(list(map(lambda j:np.random.normal(si[j], sig_sq[j], m_fake), range(n))))
	return Mf.reshape(-1), si, sig_sq

def SGLD_grad_logp2Mf(si, sig_sq, beta, O, Of, Mf, U, Uf, V, Mpure, miu_av, miu_in, lambda_reg, k, w = None):
	dR_dMf = grad_altmin_R2Mf(O, Of, U, Uf, V, Mpure, miu_av, miu_in, lambda_reg, k, w = None)
	Mfd = dense_mat(Mf, Of)
	m_fake, n = Of.shape
	Si = np.tile(si.reshape(1,-1), [m_fake,1])
	grad = -(Mfd - Si).dot(np.diag(1/(sig_sq**2))) + beta * dR_dMf
	return grad, Mfd

def SGLD_grad_logp2Mf(si, sig_sq, beta, O, Of, Mf, U, Uf, V, Mpure, miu_av, miu_in, lambda_reg, sigma, smooth, w = None):
	dR_dMf = grad_SVT_R2Mf(O, Of, U, Uf, V, sigma, Mpure, miu_av, miu_in, lambda_reg, smooth, w = None)
	Mfd = dense_mat(Mf, Of)
	m_fake, n = Of.shape
	Si = np.tile(si.reshape(1,-1), [m_fake,1])
	grad = -(Mfd - Si).dot(np.diag(1/(sig_sq**2))) + beta * dR_dMf
	return grad, Mfd

def SGLD_update(Mfd, Of, grad_logp2Mf, lr, miu_av, miu_in, B, sc_max):
	"""
	should be dense here since the projection is executed at final step
	"""
	# gradient, Mfd = SGLD_grad_altmin_logp2Mf(Mf, Of, grad_R2Mf, lr, miu_av, miu_in, B, sc_max)
	#TODO whether this is correct
	perturb = np.random.normal(0, lr, Of.shape)
	Mf_new = Mfd + 0.5*lr*grad_logp2Mf + perturb
	return Mf_new.reshape(-1)

def SGLD_attack_altmin(M, O, fake_rate, lambda_reg, lr, B, sc_max, beta, miu_av, miu_in, k, T, p, epoch):
	"""
	attack altmincomplete algorithm
	M			: data of observed matrix
	O			: indices of observed entries
	fake_rate	: rate of fake users (to orig users' num)
	lambda_reg 	: regularization parameter
	lr			: learning rate
	B			: maxinum size that could be modified
	sc_max		: maxinum permitted abs of score
	beta		: the tuning parameter to balance the detectabilility and utility
	miu_av		: weight scalar of availabilility loss in total loss	
	miu_in		: weight scalar of integrity loss in total loss
	k			: rank of the factor matrices U and V
	T			: training time
	p			: prob of observing an element
	cvg_thresh	: convergence threshold
	"""
	#init fake M~
	# import ipdb; ipdb.set_trace()
	m, n = O.shape
	fk_num = int(np.floor(m*fake_rate))
	Upure, Vpure = altmin(M, O, T, k, p, lambda_reg, lambda_reg)
	Mpure = Upure.dot(Vpure.T)
	Mf, si, sig_sq = init_M_fake_SGLD(M, O, fk_num)
	Of = np.ones((fk_num, n))
	Of = Of == 1
	w = None
	if miu_in:
		w = np.zeros(n)
		wanted_id = np.random.randint(n)
		w[wanted_id] = 2
	for itr in range(epoch):
		Msyn = np.concatenate((M,Mf))
		Osyn = np.concatenate((O,Of), axis = 0)
		Usyn, V = altmin(Msyn, Osyn, T, k, p, lambda_reg, lambda_reg)
		Mpred = Usyn[:m].dot(V.T)
		rmse_score = rmse_attack(Mpred, Mpure, O)
		print("rmse_score :", rmse_score)
		grad_logp2Mf, Mfd = SGLD_grad_logp2Mf(si, sig_sq, beta, O, Of, Mf, Usyn[:m], Usyn[m:], V, Mpure, miu_av, miu_in, lambda_reg, k, w)
		Mf_new = SGLD_update(Mfd, Of, grad_logp2Mf, lr, miu_av, miu_in, B, sc_max)
		Mf = Mf_new
	Mf = proj2feasible(Mf, B, sc_max)
	return Mf

def SGLD_attack_SVT(M, O, fake_rate, lambda_reg, delta, lr, B, sc_max, beta, miu_av, miu_in, T, r_inc, tol, smooth, epoch):
	"""
	attack altmincomplete algorithm
	M			: data of observed matrix
	O			: indices of observed entries
	fake_rate	: rate of fake users (to orig users' num)
	lambda_reg 	: regularization parameter
	lr			: learning rate
	B			: maxinum size that could be modified
	sc_max		: maxinum permitted abs of score
	beta		: the tuning parameter to balance the detectabilility and utility
	miu_av		: weight scalar of availabilility loss in total loss	
	miu_in		: weight scalar of integrity loss in total loss
	k			: rank of the factor matrices U and V
	T			: training time
	p			: prob of observing an element
	cvg_thresh	: convergence threshold
	"""
	#init fake M~
	# import ipdb; ipdb.set_trace()
	m, n = O.shape
	fk_num = int(np.floor(m*fake_rate))
	# SVT(M,O,lambda_reg,delta,T,r_inc, tol, verbose = False)
	Upure, Spure, Vpure = SVT(M,O,lambda_reg,delta,T,r_inc, tol, verbose = False)
	Mpure = Upure.dot(Spure).dot(Vpure.T)
	Mf, si, sig_sq = init_M_fake_SGLD(M, O, fk_num)
	Of = np.ones((fk_num, n))
	Of = Of == 1
	w = None
	if miu_in:
		w = np.zeros(n)
		wanted_id = np.random.randint(n)
		w[wanted_id] = 2
	for itr in range(epoch):
		Msyn = np.concatenate((M,Mf))
		Osyn = np.concatenate((O,Of), axis = 0)
		Usyn, S, V = SVT(Msyn, Osyn,lambda_reg,delta,T,r_inc, tol, verbose = False)
		Mpred = Usyn[:m].dot(S).dot(V.T)
		rmse_score = rmse_attack(Mpred, Mpure, O)
		print("rmse_score :", rmse_score)
		S = np.diag(S)
		grad_logp2Mf, Mfd = SGLD_grad_logp2Mf(si, sig_sq, beta, O, Of, Mf, Usyn[:m], Usyn[m:], V, Mpure, miu_av, miu_in, lambda_reg, S, smooth,w)
		Mf_new = SGLD_update(Mfd, Of, grad_logp2Mf, lr, miu_av, miu_in, B, sc_max)
		Mf = Mf_new
	Mf = proj2feasible(Mf, B, sc_max)
	return Mf

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Train predicting model using AltMinComplete.')
	parser.add_argument("-epoch", type=int, default=20, dest="E", help="Epoch number of attack.")
	parser.add_argument("-turns", type=int, default=20, dest="T", help="AltMinComplete algorithm will run T times.")
	parser.add_argument("-faker", type=float, default=0.2, dest="FKR", help="Fake user rate. Add FKR*user_num fake users")
	parser.add_argument("-reg", type=float, default=-1, dest="lbd", help="Regularization constant for U and V")
	parser.add_argument("-lr", type=float, default=1e-3, help="Learning rate for AltMinComplete")
	parser.add_argument("-k", type=int, default=20, help="Latent factor dimension")
	parser.add_argument("-p", type=float, default=1, help="Portion of samples sampled each iteration")
	parser.add_argument("-b", type=int, default=100, dest="B", help="Every fake user can rate at most B movies.")
	parser.add_argument("-max", type=int, default=2, dest="A", help="Max permitted abs score for fake users to rate on movies.")
	parser.add_argument("-mu1", type=float, default=1, dest="muAv", help="Weight scalar of availabilility loss")
	parser.add_argument("-mu2", type=float, default=0, dest="muIn", help="Weight scalar of integrity loss")
	parser.add_argument("-th", type=float, default=1e-5, dest="THRESH", help="Threshold of convergence")
	parser.add_argument("-beta", type=float, default=0.6, dest="BETA", help="Beta to mimic normal users")
	parser.add_argument("-ri", type=int, default=5, dest="RINC", help="Increment of r")
	parser.add_argument("-tol", type=float, default=1e-2, dest="TOL", help="Tolerance of convergence")
	parser.add_argument("-smt", type=float, default=1e-3, dest="SMOOTH", help="Smoothness parameter for dV_dMf and dU~dMf in SVT gradient")
	parser.add_argument("-dlt", type=float, default=2, dest="DELTA", help="Delta used for SVT matrix complete")
	parser.add_argument("-mod", type=str, default='altmin', dest="MODEL", help="Altmin or SVT")
	import ipdb; ipdb.set_trace()
	args = parser.parse_args()
	rating, usr_id, mov_id = read_in_MovieLens_100k(data_path = "../data/ml-100k/u.data")
	u_num = np.max(usr_id) + 1
	v_num = np.max(mov_id) + 1
	omega = np.zeros((u_num, v_num))
	omega[usr_id, mov_id] = 1 # if build directly -- IndexError: too many indices for array
	omega = (omega == 1)
	# np.ranom.seed(1)
	if args.MODEL == 'altmin':
		if args.lbd < 0:
			args.lbd = 1
		Mf = SGLD_attack_altmin(rating, omega, args.FKR, args.lbd, args.lr, args.B, args.A, args.BETA, args.muAv, args.muIn, args.k, args.T, args.p, args.E)
	elif args.MODEL == 'SVT':
		if args.lbd < 0:
			args.lbd = 1000
		Mf = SGLD_attack_SVT(rating, omega, args.FKR, args.lbd, args.DELTA, args.lr, args.B, args.A, args.BETA, args.muAv, args.muIn, args.T, args.RINC, args.TOL, args.SMOOTH, args.E)
	