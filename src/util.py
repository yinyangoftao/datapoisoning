import numpy as np
def read_in_MovieLens_1M(data_path = "ml-1m/ratings.dat"):
	data = []
	row = []
	col = []
	with open(data_path, "r") as fr:
		recs = fr.readlines()
		for i, rec in enumerate(recs):
			rec = rec.strip()
			rec = [int(item) for item in rec.split("::")]
			data += [rec[2]]
			#- 1 since id in dataset starts from 1
			row += [rec[0]-1]
			col += [rec[1]-1]
	data = np.array(data)
	row = np.array(row)
	col = np.array(col)
	# data -= 2
	return data, row, col

def read_in_MovieLens_100k(data_path = "../data/ml-100k/u.data"):
	# import ipdb; ipdb.set_trace()
	data = []
	row = [] # user
	col = [] # movies
	with open(data_path, "r") as fr:
		recs = fr.readlines()
		for i, rec in enumerate(recs):
			rec = rec.strip()
			rec = [int(item) for item in rec.split()]
			# user id | item id | rating | timestamp.
			data += [rec[2]]
			#- 1 since id in dataset starts from 1
			row += [rec[0]-1]
			col += [rec[1]-1]
	# return data, row, col
	#zero mean the data:
	data = np.array(data)
	row = np.array(row)
	col = np.array(col)
	data -= 3
	return data, row, col