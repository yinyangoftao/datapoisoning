from util_torch import *
import argparse
from scipy.sparse import linalg as spLA
from scipy.sparse import coo_matrix
from scipy.sparse import dia_matrix
from util import *
from tqdm import *

def svd_on_tau(Y, s, tau, r_inc):
	"""
	singular value thresholding decomposition of Y_{k-1}
	input
		s 		: initial # of singular values
		tau:	: threshold
	return
		U (m,k)
		S vector is shifted down by tau. (k,)
		V (k,n)
	"""
	h, w = Y.shape
	U, S, V = spLA.svds(Y, k = s)
	# singular values would be in ascending order
	while S.data[0] > tau and s < np.min([h,w]):
		s += r_inc
		s = np.min([h,w,s])
		U, S, V = spLA.svds(Y, k = s)
	S -= tau
	return U, S, V

def SVT(Mdata,omega,tau,delta,maxiter,r_inc, tol, verbose = False):
	"""
	solves Finds the minimum of   tau ||X||_* + .5 ||X||_F^2 
	
	subject to P_Omega(X) = P_Omega(M)

	referenced mc-SVT code from lrslib - https://github.com/andrewssobral/lrslibrary
	
	pay attention that in theory delta should be no larger than 2, tested the relaxed (also adopted in lrs) version, but not good
	when the final delta is 2.06, it's ok, but when it's around 3.17, it won't converge
	tau		: weight param of nuclear norm
	delta	: step size, here it's absolute step size instead of a relatice factor, which is diffrent from the paper and lrslib
	maxiter	: max # of iterations
	M 		: the observed sparse data matrix, notice that the size of its data/cols/rows is not the length of observed entries since some entries may be with value 0
	omega 	: the observability matrix, dense boolean
	r_inc	: increment r by this value when the SVD is not good enough
	"""
	# max singular value
	#emperically set tau
	h, w = omega.shape
	# tau *= np.sqrt(h*w)
	# delta *= h*w/np.count_nonzero(omega)
	usr_id, mov_id = np.nonzero(omega)
	M = coo_matrix((Mdata, (usr_id, mov_id)), dtype=np.float, shape = omega.shape)
	_, s, _ = spLA.svds(M, k=1)
	M_2_norm = s[0]
	M_F_norm = spLA.norm(M,'fro')
	k0 = np.ceil(tau/(delta*M_2_norm))
	print("finally, delta is {0:.4f}, tau is {1:.4f}, k0 is {2}".format(delta, tau, k0))
	# Y0
	y = k0*delta*M
	Y = y.copy()
	r = [] #modify r to be a length instead
	for i in tqdm(range(maxiter)):
		s = len(r) + 1
		U, Svec, V = svd_on_tau(Y, s, tau, r_inc)
		r = np.where(Svec > 0)[0]
		U = U[:,r]
		V = V[r]
		#if use sparse here, would be super slow
		S = np.diag(Svec[r])
		Xo = U.dot(S).dot(V)    #observed X
		Xo[omega == False] = 0
		Xo = coo_matrix(Xo)
		XMdiff = M - Xo
		err_F_norm = spLA.norm(XMdiff, 'fro')
		rel_err = err_F_norm/M_F_norm
		if verbose:
			print('err:', rel_err, 'r len:', len(r))
		# if converge
		if (rel_err < tol):
			break
		Y += delta*XMdiff
	print('err of matrix completion: {0:.4f}', rel_err)
	return U, S, V.T

if __name__ == "__main__":
	# parser = argparse.ArgumentParser(description='Train predicting model using AltMinComplete.')
	# parser.add_argument("-itr", type=int, default=100, dest="T", help="SVT algorithm will run at most T times.")
	# parser.add_argument("-t", type=float, default=1000, dest="TAU", help="Tau is the weight of nuclear norm term. we may set it to 5*sqrt(w*h), the smaller tau cause less accurate svd_on_tau value")
	# parser.add_argument("-d", type=float, default=2, dest="DELTA", help="step size")
	# parser.add_argument("-l", type=int, default=5, dest="R_INC", help="increment value every time used for SVD of Y_{k-1}")
	# parser.add_argument("-tl", type=float, default=1e-2, dest="TOL", help="Tolerance of convergence")
	# parser.add_argument("-v", type=bool, default=True, help="verbose mode or not")
	# import ipdb; ipdb.set_trace()
	# args = parser.parse_args()
	# rating, usr_id, mov_id = read_in_MovieLens_100k(data_path = "../data/ml-100k/u.data")
	# u_num = np.max(usr_id) + 1
	# v_num = np.max(mov_id) + 1
	# omega = np.zeros((u_num, v_num))
	# omega[usr_id, mov_id] = 1 # if build directly -- IndexError: too many indices for array
	# omega = (omega == 1)
	# X = SVT(rating, omega, args.TAU, args.DELTA, args.T, args.R_INC, args.TOL, args.v)

	n1 = 1000; n2 = 1000; r = 10
	M = np.random.randn(n1,r).dot(np.random.randn(r,n2))
	df = r*(n1+n2-r)
	oversampling = 5 
	m = min(5*df,(int)(0.99*n1*n2))
	p  = m/(n1*n2)
	print(p)
	omega_r = np.random.permutation(n1*n2)[:m]
	om_r = omega_r // n2
	om_c = omega_r % n2
	omega = np.zeros((n1,n2))
	omega[om_r, om_c] = 1
	omega = (omega == 1)
	data = M[om_r, om_c]
	sigma = 0
	sigma = 0.05*np.std(data)
	data = data + sigma*np.random.randn(len(data))
	tau = 5*np.sqrt(n1*n2)
	delta = 0.2/p
	X = SVT(data, omega, tau, delta, 100, 5, 1e-4, True)