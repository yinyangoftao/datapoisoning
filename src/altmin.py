from util import *
import argparse
import numpy as np
import sys
# from scipy.sparse import coo_matrix
from scipy.sparse.linalg import svds
from tqdm import *
# from predictor.generator import riter
# TODO try with torch, if not good enough then try multiprocess to parallelize this implementation
# TODO here dense omega is used since it's conveniet to extract nonzero entries of each lines(as well as cols)
# may want to change it to a "list of nonzero entries" version to support larger dataset

# decompose a partially visible (m,n) matrix
# difference between this and the paper: no orthogonalization
def rmse(data, M, U, V):
	m, n = M.shape
	recons = U.dot(V.T)
	recons = recons[M]
	diff = recons - data
	return np.sqrt(np.sum((diff) ** 2) / len(data))

def sample(data, O, p, n):
	"""
	sample from O into n sets, with prob p
	"""
	row, col = np.nonzero(O)
	cutoff = int(len(row)*p)
	idx = np.arange(len(row))
	for i in range(n):
		np.random.shuffle(idx)
		Mc = np.zeros(O.shape)
		Mc[row[idx[:cutoff]], col[idx[:cutoff]]] = 1
		Md = np.zeros(O.shape)
		Md[row[idx[:cutoff]], col[idx[:cutoff]]] = data[idx[:cutoff]]
		yield Md, Mc

def solve_row(U_i, V, data_i, omega_i, k, lambda_u):
	"""
	we should have U_i.dot(V[:,omega_i]) = M_i
	omega_i: observable set of row i, (|Omega_i|,)
	U_i: the current U_i to optimize (,1)
	V:   here considered as const, (n, k)
	M_i: observed elems in i-th row of matrix M, (1, |Omega_i|)
	"""
	omega_i = np.nonzero(omega_i)[0]
	if len(omega_i) == 0:
		return U_i
	# if not invertible, don't modify
	V_i = V[omega_i, :] #(|Omega_i|, k)
	#in case the length is 1 and it degenerates to a vector?
	AtA = V_i.T.dot(V_i) + lambda_u*np.eye(k)#(k, k)
	if np.linalg.cond(AtA) < 1/sys.float_info.epsilon: # invertible
		Aty = V_i.T.dot(data_i[omega_i])
		# AtA += lambda_u*np.eye(k)
		return np.linalg.solve(AtA, Aty)
	return U_i


def opt_U(U,V,data,omega,k,lambda_u):
	"""
	given V and M, want to find U that minimize || U.dot(V.T) - M ||^2
	Omega is an array of vector, each elem is a vector indicating Observable rows
	U:current U, to be updated in this function (m, k)
	V:considered as const here, (n,k)
	M:the partially observed matrix we want to re-construct (m, n)
	Omega:Omega[i] is the obervable-cols vector of row i
	note: pytorch batch LU is not good since 
	"""
	U = list(map(lambda i: solve_row(U[i,:], V, data[i], omega[i], k, lambda_u), range(U.shape[0])))
	U = np.array(U)
	return U

def altmin(data, M, T, k, p, lU, lV, verbose = False):
	"""
	factorize M into UV
	k:the rank of fator U and V
	T:number of iteration
	Omega: indicator of observablility, a sparse 0-1 matrix, elem at 1 entries observable
	M: values of the partially observable matrix, indices indicated by Omega
	p		: prob of sampling 
	"""
	# m, n = M.shape
	# P_omg0_M = np.zeros((m,n))
	for i, Mt in enumerate(tqdm(sample(data, M, p, 2*T+1))):
		data_t, omega_t = Mt
		if i == 0:
			u,s,v = svds(data_t/p, k=k)
			U = u.copy()
			V = v.T
		else:
			# for t in range(T):
			# Mt = Mt.toarray()
			if verbose:
				print('re-cons error:',rmse(data, M, U, V))
			if i%2:
				V = opt_U(V,U,data_t.T,omega_t.T,k,lV)
			else:
				U = opt_U(U,V,data_t,omega_t,k,lU)
	print('final re-cons error:',rmse(data, M, U, V))
	return U,V
	
if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Train predicting model using AltMinComplete.')
	parser.add_argument("--turns", type=int, default=20, dest="T", help="AltMinComplete algorithm will run T times.")
	parser.add_argument("--lu", type=float, default=1, dest="lU", help="Regularization constant for U")
	parser.add_argument("--lv", type=float, default=1, dest="lV", help="Regularization constant for V")
	parser.add_argument("--k", type=int, default=20, help="Latent factor dimension")
	parser.add_argument("--p", type=float, default=0.99, help="Portion of samples sampled each iteration")
	# parser.add_argument("--plot", action='store_true', help="Do you want to plot for each fold?")
	import ipdb; ipdb.set_trace()
	args = parser.parse_args()
	rating, usr_id, mov_id = read_in_MovieLens_100k(data_path = "../data/ml-100k/u.data")
	u_num = np.max(usr_id) + 1
	v_num = np.max(mov_id) + 1
	# M = coo_matrix((rating, (usr_id, mov_id)), dtype=np.float, shape = (u_num, v_num))
	# M = M.todense()
	Omega = np.zeros((u_num, v_num))
	Omega[usr_id, mov_id] = 1 # if build directly -- IndexError: too many indices for array
	Omega = (Omega == 1)
	U, V = altmin(rating, Omega, args.T, args.k, args.p, args.lU, args.lV, True)
	print(U.shape)
