import numpy as np
from scipy.sparse import coo_matrix
# from scipy.sparse.linalg import svds
from svt_pysp import *
import argparse
from util import *
from util_np import *
from altmin import altmin

def init_M_fake_PGA(m_fake, n, B, sc_max):
	"""
	both ratings and rated items randomly sampled
	TODO perhaps want to return a dense Mf, but anyway the omega matrix is necessary since 0 is also a valid rating value
	TODO modify omega to be boolean
	n:	number of all items
	:return:
	Mf:data of fake matrix (since sparse, here we use a vector of its nonzero entries)
	Of:indices of nonzero entries in the fake matrix, dense: (m_fake,n)
	"""
	Mf = []
	Of = np.zeros((m_fake, n))
	rate_idx = np.arange(n)
	for i in range(m_fake):
		np.random.shuffle(rate_idx)
		Of[i, rate_idx[:B]] = 1
		ratings = np.random.randint(-sc_max, sc_max + 1, size = (B,))
		Mf += [ratings]
	Of = Of == 1
	Mf = np.concatenate(Mf)
	return Mf, Of
	
def pga_update(Mf, Of, grad_R2Mf, lr, miu_av, miu_in, B, sc_max):
	# build dense M_fake
	# m_fake, n = Of.shape
	# cx, cy = np.nonzero(Of)
	Mf_dense = dense_mat(Mf, Of)
	Mf_dense += lr*grad_R2Mf
	Mf_new, Of_new = proj2feasible(Mf_dense, B, sc_max)
	return Mf_new, Of_new

def attack_AltMin_PGA(M, O, fake_rate, lambda_reg, lr, B, sc_max, miu_av, miu_in, k, T, p, epoch, cvg_thresh):
	"""
	attack altmincomplete algorithm using PGA
	M			: data of observed matrix
	O			: indices of observed entries
	fake_rate	: rate of fake users (to orig users' num)
	lambda_reg 	: regularization parameter used for altmin 
	lr			: learning rate
	B			: maxinum size that could be modified
	sc_max		: maxinum permitted abs of score
	miu_av		: weight scalar of availabilility loss in total loss	
	miu_in		: weight scalar of integrity loss in total loss
	# wgt_util_in	: file path or policy of weight vector of items(movies) of integrity utility, shouldn't be None when miu_in not 0
	k			: rank of the factor matrices U and V
	T			: # of training epoch
	p			: prob of observing an element
	cvg_thresh	: convergence threshold
	"""
	#init fake M~
	m, n = O.shape
	fk_num = int(np.floor(m*fake_rate))
	Upure, Vpure = altmin(M, O, T, k, p, lambda_reg, lambda_reg)
	Mpure = Upure.dot(Vpure.T)
	Mf, Of = init_M_fake_PGA(fk_num, n, B, sc_max)
	w = None
	if miu_in:
		w = np.zeros(n)
		wanted_id = np.random.randint(n)
		w[wanted_id] = 2
	converged = False
	for itr in range(epoch):
		Msyn = np.concatenate((M,Mf))
		Osyn = np.concatenate((O,Of), axis = 0)
		Usyn, V = altmin(Msyn, Osyn, T, k, p, lambda_reg, lambda_reg)
		Mpred = Usyn[:m].dot(V.T)
		rmse_score = rmse_attack(Mpred, Mpure, O)
		print("rmse_score :", rmse_score)
		grad_R2Mf = grad_altmin_R2Mf(O, Of, Usyn[:m], Usyn[m:], V, Mpure, miu_av, miu_in, lambda_reg, k, w)
		Mf_new, Of_new = pga_update(Mf, Of, grad_R2Mf, lr, miu_av, miu_in, B, sc_max)
		if converged:
			break
		if converge(Mf, Of, Mf_new, Of_new, cvg_thresh):
			converged = True
		Mf = Mf_new
		Of = Of_new
	return Mf_new, Of_new

def attack_SVT_PGA(M, O, fake_rate, lambda_reg, delta, lr, B, sc_max, miu_av, miu_in, T, r_inc, tol, smooth, epoch, cvg_thresh):
	"""
	attack altmincomplete algorithm using PGA
	M			: data of observed matrix
	O			: indices of observed entries
	fake_rate	: rate of fake users (to orig users' num)
	lambda_reg 	: regularization parameter, which is tau for SVT
	delta 		: step length used for SVT
	lr			: learning rate
	B			: maxinum size that could be modified
	sc_max		: maxinum permitted abs of score
	miu_av		: weight scalar of availabilility loss in total loss	
	miu_in		: weight scalar of integrity loss in total loss
	# wgt_util_in	: file path or policy of weight vector of items(movies) of integrity utility, shouldn't be None when miu_in not 0
	T			: # of training epoch
	r_inc		: # of r increment when computing SVD of Y_{k-1} in SVT
	tol 		: tolerance for convergence used in SVT
	cvg_thresh	: convergence threshold
	"""
	#init fake M~
	import ipdb; ipdb.set_trace()
	m, n = O.shape
	fk_num = int(np.floor(m*fake_rate))
	#keep a copy of unpoisoned data here for comparison
	Upure, Spure, Vpure = SVT(M,O,lambda_reg,delta,T,r_inc, tol, verbose = False)
	Mpure = Upure.dot(Spure).dot(Vpure.T)
	#initialize M~0
	Mf, Of = init_M_fake_PGA(fk_num, n, B, sc_max)
	w = None
	if miu_in:
		w = np.zeros(n)
		wanted_id = np.random.randint(n)
		w[wanted_id] = 2
	converged = False
	for itr in range(epoch):
		Msyn = np.concatenate((M,Mf))
		Osyn = np.concatenate((O,Of), axis = 0)
		Usyn, S, V = SVT(Msyn, Osyn,lambda_reg,delta,T,r_inc, tol, verbose = False)
		Mpred = Usyn[:m].dot(S).dot(V.T)
		rmse_score = rmse_attack(Mpred, Mpure, O)
		print("rmse_score :", rmse_score)
		S = np.diag(S)
		grad_R2Mf = grad_SVT_R2Mf(O, Of, Usyn[:m], Usyn[m:], V, S, Mpure, miu_av, miu_in, lambda_reg, smooth, w)
		Mf_new, Of_new = pga_update(Mf, Of, grad_R2Mf, lr, miu_av, miu_in, B, sc_max)
		if converged:
			break
		if converge(Mf, Of, Mf_new, Of_new, cvg_thresh):
			converged = True
		Mf = Mf_new
		Of = Of_new
	return Mf_new, Of_new

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Train predicting model using AltMinComplete.')
	parser.add_argument("-epoch", type=int, default=20, dest="E", help="Epoch number of attack.")
	parser.add_argument("-turns", type=int, default=500, dest="T", help="Inner matrix complete algorithm will run T times.")
	parser.add_argument("-faker", type=float, default=0.2, dest="FKR", help="Fake user rate. Add FKR*user_num fake users")
	parser.add_argument("-reg", type=float, default=-1, dest="lbd", help="Regularization constant, for altmin, this is for U and V's norm, for SVT this is a large weight of nuclear norm")
	parser.add_argument("-lr", type=float, default=1e-3, help="Learning rate for AltMinComplete")
	parser.add_argument("-k", type=int, default=20, help="Latent factor dimension")
	parser.add_argument("-p", type=float, default=1, help="Portion of samples sampled each iteration")
	parser.add_argument("-b", type=int, default=100, dest="B", help="Every fake user can rate at most B movies.")
	parser.add_argument("-max", type=int, default=2, dest="A", help="Max permitted abs score for fake users to rate on movies.")
	parser.add_argument("-mu1", type=float, default=1, dest="muAv", help="Weight scalar of availabilility loss")
	parser.add_argument("-mu2", type=float, default=0, dest="muIn", help="Weight scalar of integrity loss")
	parser.add_argument("-th", type=float, default=1e-5, dest="THRESH", help="Threshold of convergence")
	parser.add_argument("-mod", type=str, default='altmin', dest="MODEL", help="Model to attack")
	parser.add_argument("-ri", type=int, default=5, dest="RINC", help="Increment of r")
	parser.add_argument("-tol", type=float, default=1e-2, dest="TOL", help="Tolerance of convergence")
	parser.add_argument("-smt", type=float, default=1e-3, dest="SMOOTH", help="Smoothness parameter for dV_dMf and dU~dMf in SVT gradient")
	parser.add_argument("-dlt", type=float, default=2, dest="DELTA", help="Delta used for SVT matrix complete")
	# import ipdb; ipdb.set_trace()
	args = parser.parse_args()
	rating, usr_id, mov_id = read_in_MovieLens_100k(data_path = "../data/ml-100k/u.data")
	u_num = np.max(usr_id) + 1
	v_num = np.max(mov_id) + 1
	omega = np.zeros((u_num, v_num))
	omega[usr_id, mov_id] = 1 # if build directly -- IndexError: too many indices for array
	omega = (omega == 1)
	# np.ranom.seed(1)
	if args.MODEL == 'altmin':
		if args.lbd < 0:
			args.lbd = 1
		Mf = attack_AltMin_PGA(rating, omega, args.FKR, args.lbd, args.lr, args.B, args.A, args.muAv, args.muIn, args.k, args.T, args.p, args.E, args.THRESH)
	elif args.MODEL == 'SVT':
		if args.lbd < 0:
			args.lbd = 1000
		Mf = attack_SVT_PGA(rating, omega, args.FKR, args.lbd, args.DELTA, args.lr, args.B, args.A, args.muAv, args.muIn, args.T, args.RINC, args.TOL, args.SMOOTH, args.E, args.THRESH)