import numpy as np

def rmse_attack(poisoned, pure, O):
	"""
	calculate average utility loss
	poisoned: the poisoned prediction, (m, n)
	pure	: prediction based on pure data
	O		: indices of observed M, sparse
	"""
	m, n = O.shape
	M_row, M_col = np.nonzero(O)
	diff = poisoned - pure
	diff[M_row, M_col] = 0
	return np.sqrt(np.sum(diff**2)/(m*n - len(M_row)))

def dense_mat(Mf, Of):
	Mf_dense = np.zeros((Of.shape))
	Mf_dense[np.nonzero(Of)] = Mf
	return Mf_dense

def converge(old_Mf, old_Of, Mf, Of, threshold):
	old_Mf_dense = dense_mat(old_Mf, old_Of)
	Mf_dense = dense_mat(Mf, Of)
	diff = (old_Mf_dense - Mf_dense)**2
	meandiff = np.sqrt(np.sum(diff)/(np.count_nonzero(old_Of) + np.count_nonzero(Of)))
	print("difference:", meandiff)
	return meandiff < threshold

def proj2feasible(Mf_dense, B, sc_max):
	"""
	project onto feasible set, each row at most B nonzero items, and each entry with abs no larger than sc_max
	"""
	abs_Mf = -np.abs(Mf_dense)
	idx = np.argpartition(Mf_dense, B)
	idx = idx[:,:B]
	Mf_new = np.zeros(Mf_dense.shape)
	Of_new = np.zeros(Mf_dense.shape)
	m_fake, n = Mf_dense.shape
	for i in range(m_fake):
		Mf_new[i, idx[i]] = Mf_dense[i, idx[i]]
		Of_new[i, idx[i]] = 1
	Mf_new = np.clip(Mf_new, -sc_max, sc_max)
	Of_new = Of_new == 1
	cx, cy = np.nonzero(Of_new)
	return Mf_new[cx, cy], Of_new

#TODO currently all data matrices are represented by a dense mat of indices and a vector representing datas

def grad_R2Mpred_av(Mpred, Mpure, omega):
	"""
	gradient of availabilility utility to Mpred, the prediction of M
	"""
	diff = Mpred - Mpure
	diff[omega] = 0
	diff = 2*diff
	return diff

def grad_R2Mpred_in(w, m):
	"""
	gradient of availabilility utility to Mpred, the prediction of M
	w:	weight vector (n,)
	m:	num of normal user
	"""
	return np.tile(w,[m,1])

def grad_altmin_R2V(dR_dMpred, U):
	"""
	actually should use 3d tensor here. but since d(Mik)/dVj = 0 when k != j, we might as well use 2d matrix here
	dR_dMpred	: (m, n) gradient of utility to attacked prediction of M
	U			: (m, k) normal user embedding
	V			: (n, k) items embedding
	return:
	gradU:(m+m_fake, k)
	gradV:(n, k)
	"""
	dR_dV = dR_dMpred.T.dot(U)
	return dR_dV

def grad_SVT_R2U(dR_dMpred, V, sigma):
	"""
	actually should use 3d tensor here. but since d(Mik)/dVj = 0 when k != j, we might as well use 2d matrix here
	dR_dMpred	: (m, n) gradient of utility to attacked prediction of M
	V			: (n, k) items embedding
	sigma 		: (k, ) diagonal matrix of singular values
	return:
	dR_dU 		:(m, k)
	"""
	dR_dU = dR_dMpred.dot(V).dot(np.diag(sigma))
	return dR_dU

def grad_SVT_R2V(dR_dMpred, U, sigma):
	"""
	actually should use 3d tensor here. but since d(Mik)/dVj = 0 when k != j, we might as well use 2d matrix here
	dR_dMpred	: (m, n) gradient of utility to attacked prediction of M
	U			: (m, k) normal user embedding
	sigma 		: (k, k) diagonal matrix of singular values
	return:
	dR_dV 		: (n, k)
	"""
	dR_dV = dR_dMpred.T.dot(U).dot(np.diag(sigma))
	return dR_dV

def grad_SVT_R2sigma(dR_dMpred, U, V):
	"""
	dR_dMpred	: (m, n) gradient of utility to attacked prediction of M
	U			: (m, k) normal user embedding
	V			: (n, k) items embedding
	return:
	dR_dsigma	: (k, )
	"""
	#(k, 1, m) (k, m, n) (k, n, 1)
	res = np.matmul(np.expand_dims(U.T, axis = 1), np.expand_dims(dR_dMpred, axis = 0))
	res = np.matmul(res, np.expand_dims(V.T, axis = 2))
	return np.squeeze(res)

def inv_term_V_j(Osyn_j, Usyn):
	"""
	sigmaV of v_j
	use whole synthesized U matrix when computing sigma
	return:sigmaV_j, (k,k)
	"""
	observed_rows = np.nonzero(Osyn_j)[0]
	u_valid = Usyn[observed_rows,:]	#(|omega_j|,k)
	return u_valid.T.dot(u_valid)

# def grad_altmin_Vj2Mfij(Osyn_j, Of_j, Usyn, Uf):
# 	"""
# 	return dVj_dMfij:(m_fake, k)
# 	"""
# 	sigVj = sigmaV_j(Osyn_j, Usyn) #(k,k)
# 	#should be valid to only observed malicious users, namely, i in Of_j
# 	U_j = np.zeros((Uf)) #(m_fake, k)
# 	U_j[Of_j] = Uf[Of_j]
# 	return U_j.dot(sigVj)

def inv_term_V(Osyn, Usyn, lambda_reg, k):
	"""
	return (n,k,k), the inverse sol for every possible 
	"""
	lamb_eye_k = lambda_reg*np.eye(k)
	return np.stack(list(map(lambda j: np.linalg.inv(inv_term_V_j(Osyn[:,j], Usyn) + lamb_eye_k), range(Osyn.shape[1]))))

def Uf_valid_j(Of_j, Uf):
	"""
	return	:(m_fake, k)
	a matrix using Uf components for rows in omega_j, zero for other rows (n, m_fake, k)
	"""
	U_j = np.zeros((Uf.shape)) #(m_fake, k)
	U_j[Of_j] = Uf[Of_j]
	return U_j

def Uf_valid(Of, Uf):
	"""
	return (n, m_fake, k)
	"""
	return np.stack(list(map(lambda j: Uf_valid_j(Of[:,j], Uf), range(Of.shape[1]))))

def grad_altmin_V2Mf(Usyn, Uf, Osyn, Of, lambda_reg, k):
	"""
	return dV_dMf:(n, m_fake, k)
	"""
	Uf_vld = Uf_valid(Of, Uf) #(n, m_fake, k)
	inv_V = inv_term_V(Osyn, Usyn, lambda_reg, k) #(n, k, k)
	return np.matmul(Uf_vld, inv_V)


# def grad_SVT_Uf_i_Mf_il(V, omega_i, l, sigma, lambda_reg, smooth):
# 	"""
# 	return dUf_i_d_Mf_il: (k,1)
# 	"""
# 	#build V_il, and notice that omega_i won't change outside since it's call by ref
# 	k = len(sigma)
# 	omega_i[l] = True
# 	V_il = V[omega_i].reshape(k,-1) # (k by r)
# 	# Ai transpose
# 	Ai_t = np.diag(sigma + lambda_reg).dot(V_il)
# 	res = np.linalg.inv(Ai_t.dot(Ai_t.T) + smooth*np.eye(k)).dot(np.diag(sigma + lambda_reg)).dot(V[l])
# 	return np.expand_dims(res, axis = 1)

def grad_SVT_U2Mf(sigma, smooth, lambda_reg, V, Of):
	"""
	actually this is useless since dMpred_dUf = 0, and dU_dMf = 0 -- so gradient from both U and Uf has 0 part
	return
		dU_dMf:
		(m', n, k, 1), derivative of Ufake to Mfake
	"""
	pass
	# m_fake = Of.shape[0]
	# n = V.shape[0]
	# return np.stack(list(map(lambda i: np.stack(list(map(lambda j: grad_SVT_Uf_i_Mf_il(V, Of[i], j, sigma, lambda_reg, smooth), range(n)))), range(m_fake))))

def grad_SVT_Vj_Mf_lj(Usyn, Osyn_j, l, sigma, lambda_reg, smooth):
	"""
	return dVj_Mf_lj: (k,1)
	"""
	#build U_il, and notice that omega_i won't change outside since it's call by ref
	k = len(sigma)
	Osyn_j[l] = True
	U_jl = Usyn[Osyn_j].reshape(k,-1) # (k by r)
	Bj = U_jl.T.dot(np.diag(sigma + lambda_reg)) #(r, k)
	res = np.linalg.inv(Bj.T.dot(Bj) + smooth*np.eye(k)).dot(np.diag(sigma + lambda_reg)).dot(Usyn[l])
	return np.expand_dims(res, axis = 1)

def grad_SVT_V2Mf(sigma, smooth, lambda_reg, Usyn, Osyn, m):
	"""
	return
		dV_dMf:
		(m', n, k, 1), derivative of Ufake to Mfake
	"""
	total_m, n = Osyn.shape
	return np.stack(list(map(lambda i: np.stack(list(map(lambda j: grad_SVT_Vj_Mf_lj(Usyn, Osyn[:,j], i, sigma, lambda_reg, smooth), range(n)))), range(m, total_m))))


def grad_SVT_Sigma2Mf(Uf, V):
	"""
	Uf 	: m', k
	V 	: n, k
	return 
		dSigma_dMf:(k, m', n)
	"""
	#(k, m',1)(k, 1, n)
	return 1/np.matmul(np.expand_dims(Uf.T, axis = 2), np.expand_dims(V.T, axis = 1))

def grad_altmin_R2Mf(O, Of, U, Uf, V, Mpure, miu_av, miu_in, lambda_reg, k, w = None):
	"""
	O		: indices of observed entries of normal users' matrix
	Osyn	: indices of observed entries of malicious users' matrix
	U		: (m,k)
	Uf		: (m_fake, k)
	V		: (n,k)
	Mpure	: prediction of M based on normal users' ratings
	@return :
	grad_R2Mf	: gradient of utility to M_fake (m_fake, n)
	"""
	Mpred = U.dot(V.T)
	m, n = O.shape
	dR_dMpred = np.zeros((O.shape))
	assert (miu_in != 0 or miu_av != 0) and "at least one of miu_av and miu_in should be nonzero!"
	if miu_av:
		dR_dMpred += grad_R2Mpred_av(Mpred, Mpure, O)
	if miu_in:
		assert (not(w is None)) and "w should be given when miu_av is nonzero!"
		dR_dMpred += grad_R2Mpred_in(w, m)
	dR_dV = grad_altmin_R2V(dR_dMpred, U) #(n, k)
	dR_dV = np.expand_dims(dR_dV,axis = 2)	#(n, k, 1)
	dV_dMf = grad_altmin_V2Mf(np.vstack([U,Uf]), Uf, np.vstack([O,Of]), Of, lambda_reg, k) #(n, m_fake, k)
	dR_dMf = np.matmul(dV_dMf, dR_dV) #(n, m_fake, 1)
	dR_dMf = np.squeeze(dR_dMf).T
	return dR_dMf

def grad_SVT_R2Mf(O, Of, U, Uf, V, sigma, Mpure, miu_av, miu_in, lambda_reg, smooth, w = None):
	"""
	O		: indices of observed entries of normal users' matrix
	Osyn	: indices of observed entries of malicious users' matrix
	U		: (m,k)
	Uf		: (m_fake, k)
	V		: (n,k)
	Mpure	: prediction of M based on normal users' ratings
	@return :
	grad_R2Mf	: gradient of utility to M_fake (m_fake, n)
	"""
	Mpred = U.dot(V.T)
	m, n = O.shape
	dR_dMpred = np.zeros((O.shape))
	assert (miu_in != 0 or miu_av != 0) and "at least one of miu_av and miu_in should be nonzero!"
	if miu_av:
		dR_dMpred += grad_R2Mpred_av(Mpred, Mpure, O)
	if miu_in:
		assert (not(w is None)) and "w should be given when miu_av is nonzero!"
		dR_dMpred += grad_R2Mpred_in(w, m)
	# dR_dU = grad_SVT_R2U(dR_dMpred, V, sigma) #(m', k)
	# dR_dU = np.expand_dims(dR_dU, axis = 1) #(m',1,k)
	# dR_dU = np.expand_dims(dR_dU, axis = 2) #(m',1,1,k)
	dR_dV = grad_SVT_R2V(dR_dMpred, U, sigma) #(n, k)
	dR_dV = np.expand_dims(dR_dV, axis = 1) #(n,1,k)
	dR_dV = np.expand_dims(dR_dV, axis = 0) #(1,n,1,k)
	dR_dsigma = grad_SVT_R2sigma(dR_dMpred, U, V) #(k,)
	dR_dsigma = np.expand_dims(dR_dsigma, axis = 0) #(1,k)
	dR_dsigma = np.expand_dims(dR_dsigma, axis = 0) #(1,1,k)
	dR_dsigma = np.expand_dims(dR_dsigma, axis = 0) #(1,1,1,k)

	# dU_dMf = grad_SVT_U2Mf(sigma, smooth, lambda_reg, V, Of) #(m', n, k, 1)
	dV_dMf = grad_SVT_V2Mf(sigma, smooth, lambda_reg, np.vstack([U,Uf]), np.vstack([O,Of]), m)
	dSigma_dMf = grad_SVT_Sigma2Mf(Uf, V) #(k, m', n)
	dSigma_dMf = np.transpose(dSigma_dMf, (1,2,0)) #(m', n, k)
	dSigma_dMf = np.expand_dims(dSigma_dMf, axis = 3) #(m', n, k, 1)

	# dR_dMf_Uf = np.matmul(dR_dU, dU_dMf)
	dR_dMf_V = np.matmul(dR_dV, dV_dMf)
	dR_dMf_Sigma = np.matmul(dR_dsigma, dSigma_dMf)
	dR_dMf = np.squeeze(dR_dMf_V) + np.squeeze(dR_dMf_Sigma)
	return dR_dMf